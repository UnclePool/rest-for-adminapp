const express = require('express');
const bodyParser = require('body-parser');
const ObjectID = require('mongodb').ObjectID;
const eventControllers = require('./controllers/events');
const db = require('./db');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/events', eventControllers.all);

app.get('/events/:id', eventControllers.findById);

app.post('/events', eventControllers.create);

app.put('/events/:id', eventControllers.update);

app.delete('/events/:id', (req, res) => {
  db.get().collection('events').deleteOne(
    { _id: req.params.id },
    (err, result) => {
      if (err) {
        return console.log(err);
      }

      res.sendStatus(200);
    }
  )
});


db.connect('mongodb://localhost:27017/', (err) => {
  if (err) {
    return console.log(err);
  }

  app.listen(3000, () => {
    console.log('start REST API');
  })
});
