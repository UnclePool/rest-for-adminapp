const Events = require('../models/events');

exports.all = (req, res) => {
  Events.all((err, docs) => {
    if (err) {
      console.log(err);
      res.sendStatus(500);
    }

    res.send(docs);
  });
};

exports.findById = (req, res) => {
  Events.findById(req.params.id, (err, doc) => {
    if (err) {
      console.log(err);
      return res.sendStatus(500);
    }

    res.send(doc);
  });
};

exports.create = (req, res) => {
  const event = {
    eventName: req.body.eventName,
    eventAddress: req.body.eventAddress,
  };

  Events.create(event, (err, result) => {
    if (err) {
      console.log(err);
      return res.sendStatus(500);
    }
    res.send(result);
  });
};

exports.update = (req, res) => {
  Events.update(
    { _id: req.params.id },
    {
      eventName: req.body.eventName,
      eventAddress: req.body.eventAddress
    },
    (err, result) => {
      if (err) {
        return console.log(err);
      }

      res.sendStatus(200);
    }
  )
};
