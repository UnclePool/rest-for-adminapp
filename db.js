const MongoClient = require('mongodb').MongoClient;

const state = {
  db: null,
};

exports.connect = (url, done) => {
  if (state.db) {
    done();
  }

  const dbName = 'eventApi';
  MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true },  (err, client) => {
    if (err) {
      return done(err);
    }

    state.db = client.db(dbName);
    done();
  })
};

exports.get = () => {
  return state.db;
};
