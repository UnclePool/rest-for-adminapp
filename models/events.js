const db = require('../db');
const ObjectID = require('mongodb').ObjectID;

exports.all = (cb) => {
  db.get().collection('events').find().toArray((err, docs) => {
    cb(err, docs);
  });
};

exports.findById = (id, cb) => {
  db.get().collection('events').findOne(ObjectID(id), (err, doc) => {
    cb(err, doc)
  });
};

exports.create = (event, cb) => {
  db.get().collection('events').insert(event, (err, result) => {
    cb(err, result);
  });
};

exports.update = (id, newData, cb) => {
  db.get().collection('events').updateOne(
    { _id: ObjectID(id) },
    newData,
    (err, result) => {
      cb(err, result);
    });
};
